export const state = () => ({
    currentEntree: null,
    entrees: [],
    listMutants: [],
    groceryListEntree: null
})

export const getters = {
  getEntrees: (state) => {
    return state.entrees
  },
  getOrderedEntrees: (state) => {
    return state.entrees
  },
  getListMutants: (state) => {
    return state.listMutants
  },
  getGroceryIngredients: (state) => {
    let recipeIngredients = [];
    state.entrees.forEach(entree => entree.recipe.attributes.RecipeIngredients.forEach(recipeIngredient => {
      recipeIngredients.push({
        Amount: recipeIngredient.Amount,
        Ingredient: {
          attributes: {
            Name: recipeIngredient.Ingredient.data.attributes.Name,
            Category: recipeIngredient.Ingredient.data.attributes.Category
          }
        }
      });
    }));

    let allIngredients = [...recipeIngredients, ...state.listMutants].map(listEntry => {
      return {
        amount: listEntry.Amount,
        name: listEntry.Ingredient.attributes.Name,
        category: listEntry.Ingredient.attributes.Category
      }
    });
    return allIngredients;
  },
}

export const actions = {
  addEntree({commit}, entree) {
    commit('addEntree', entree)
  },
  addListMutant({commit}, listMutant) {
    commit('addListMutant', listMutant)
  },
  removeEntree({commit}, entree) {
    commit('removeEntree', entree)
  },
  removeListMutant({commit}, mutant) {
    commit('removeListMutant', mutant)
  },
  // TODO FUTURE PERSISTING ENTREE CALENDER + LIST MUTANTS | also add loadState
  async saveState({state}, persistPassword) {
    const entreePayload = JSON.stringify(state.entrees);
    const listMutantPayload = JSON.stringify(state.listMutants);
    console.log("entreePayload", entreePayload);
    console.log("listMutantPayload", listMutantPayload);
    console.log(persistPassword);
  }
}

export const mutations = {
    addEntree(state, entree) {
      state.entrees.push(entree)
    },
    addListMutant(state, listMutant) {
      state.listMutants.push(listMutant)
    },
    removeEntree(state, removeThisEntree) {
      state.entrees.splice(state.entrees.findIndex(entree => entree === removeThisEntree), 1);
    },
    removeListMutant(state, removeThisMutant) {
      state.listMutants.splice(removeThisMutant.index, 1);
    },
}
