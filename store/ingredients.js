import axios from 'axios'

export const state = () => ({
    ingredients: [],
    isLoading: true,
    categories: [
      { name: "veggie",      color: "green",  icon: "leek" },
      { name: "fruit",       color: "pink",   icon: "food-apple" },
      { name: "meat",        color: "red",    icon: "sheep" },
      { name: "seafood",     color: "blue",   icon: "fish" },
      { name: "grain/bread", color: "brown",  icon: "baguette" },
      { name: "drink",       color: "black",  icon: "glass-flute" },
      { name: "dairy",       color: "white",  icon: "food-variant" },
      { name: "other",       color: "purple", icon: "shaker" },
      { name: "non-food",    color: "blue",   icon: "bowl" },
    ],
    ingredientUnits: [
      "milligrams",
      "grams",
      "milliliters",
      "litres",
      "slices",
      "units",
      "cups",
      "teaspoons",
      "tablespoons",
      "pieces",
      "ounces",
      "fluid ounces"
    ],
})

export const getters = {
    getIngredients: (state) => {
        return state.ingredients
    },
    getLoadingStatus: (state) => {
      return state.isLoading
    },
    getCategories: (state) => {
      return state.categories;
    },
    getCategoryNames: (state) => {
      return state.categories.map(category => category.name);
    },
    getIngredientUnits: (state) => {
      return state.ingredientUnits;
    },
}
export const mutations = {
    setIngredients (state, ingredients) {
        console.log(JSON.stringify(ingredients, null, 4))
        console.log('loadingState', state.isLoading)
        state.ingredients = []
        for (const ingredient in ingredients) {
          console.log('ingredient', ingredient.attributes)
          state.ingredients.push(ingredient.attributes)
        }
        state.isLoading = false;
    },
    addIngredient (state, ingredient) {
      state.ingredients.push(ingredient);
    }
}

export const actions = {
    async refreshIngredients ({commit}) {
      state.isLoading = true;
      await axios.get("https://hiren-devs-strapi-j5h2f.ondigitalocean.app/api/ingredients")
      .then((response) => {
        commit('setIngredients', response.data.data);
      })
      .catch(error => console.log(error))
    },
    async addIngredient({dispatch}, ingredient) {
      await axios
      .post("https://hiren-devs-strapi-j5h2f.ondigitalocean.app/api/ingredients", ingredient)
      .catch(error => console.log(error))

      setTimeout(dispatch('refreshIngredients', 1500));
    },
    getCategoryStyle(categoryName) {
      return state.categories.find(category => categoryName === category.name);
    },
}
